	create table IF NOT EXISTS SETOR
	(
	   id integer not null,
	   DESCRICAO varchar(30) not null,
	   primary key(id)
	);

	create table IF NOT EXISTS COLABORADOR
	(
	   id integer auto_increment not null,
	   CPF varchar(11) not null,
	   NOME varchar(50) null,
	   TELEFONE varchar(30) null,
	   EMAIL varchar(30) null,
	   SETOR_ID integer,
	   primary key(id),
	   FOREIGN KEY (SETOR_ID) REFERENCES SETOR(ID)
	);


INSERT INTO SETOR (ID,DESCRICAO) VALUES (1, 'TI');
INSERT INTO SETOR (ID,DESCRICAO) VALUES (2, 'FATURAMENTO');
INSERT INTO SETOR (ID,DESCRICAO)  VALUES (3, 'RH');

