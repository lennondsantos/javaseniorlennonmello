package br.com.mobicare.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "COLABORADOR")
public class Colaborador {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    @NotNull
    @Column(columnDefinition = "VARCHAR(11)",nullable=false)
	private String cpf;
    @Column(columnDefinition = "VARCHAR(50)")
	private String nome;
    @Column(columnDefinition = "VARCHAR(30)")
	private String telefone;
    @Column(columnDefinition = "VARCHAR(30)")
	private String email;
    @ManyToOne
    private Setor setor;
    
    public Colaborador() {}
    
    public Colaborador(String nome,String email,String descricaoSetor) {
    	this.nome = nome;
    	this.email = email;
    	this.setor = new Setor();
    	this.setor.setDescricao(descricaoSetor);
    }
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	
	

}
