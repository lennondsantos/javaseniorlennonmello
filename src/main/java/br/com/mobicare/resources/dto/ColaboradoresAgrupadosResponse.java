package br.com.mobicare.resources.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.mobicare.model.Colaborador;

public class ColaboradoresAgrupadosResponse {
	
	private String descricao;
	private List<ColaboradorFlat> colaboradores = new ArrayList<>();
	
	private ColaboradoresAgrupadosResponse(String descricao, List<Colaborador> data) {
		this.descricao = descricao;
		data.forEach((c) -> colaboradores.add(new ColaboradorFlat(c)));
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<ColaboradorFlat> getColaboradores() {
		return colaboradores;
	}

	public void setColaboradores(List<ColaboradorFlat> colaboradores) {
		this.colaboradores = colaboradores;
	}	

	public static ColaboradoresAgrupadosResponse createInstance(String descricao, List<Colaborador> colaboradores) {
		return new ColaboradoresAgrupadosResponse(descricao, colaboradores);
	}
}
