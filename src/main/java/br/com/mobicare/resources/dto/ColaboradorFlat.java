package br.com.mobicare.resources.dto;

import br.com.mobicare.model.Colaborador;

public class ColaboradorFlat {
	
	private String nome;
	private String email;
	
	 ColaboradorFlat(Colaborador c) {
		this.nome = c.getNome();
		this.email = c.getEmail();
	}

	public String getNome() {
		return nome;
	}


	public String getEmail() {
		return email;
	}


}
