package br.com.mobicare.resources;

import static br.com.mobicare.resources.dto.ColaboradoresAgrupadosResponse.createInstance;
import static java.util.stream.Collectors.groupingBy;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mobicare.model.Colaborador;
import br.com.mobicare.resources.dto.ColaboradoresAgrupadosResponse;
import br.com.mobicare.services.ColaboradorService;


@RestController
@RequestMapping("/colaboradores")
public class ColaboradorResource {
	
	@Autowired
	ColaboradorService colaboradorService;


	@GetMapping
	public ResponseEntity<List<ColaboradoresAgrupadosResponse>> buscarTodos(){
		List<ColaboradoresAgrupadosResponse> result = new ArrayList<>();
		colaboradorService.buscarTodos()
			.stream()
			.collect(groupingBy(colaborador -> colaborador.getSetor().getDescricao()))
			.forEach((k,v) -> result.add(createInstance(k, v)));
		
		return ok(result);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Colaborador> buscarPorId(@PathVariable("id") Integer id){
		return ok(colaboradorService.buscarPor(id));
	}

	@PostMapping
	public ResponseEntity<Colaborador> criar(@Valid @RequestBody Colaborador colaborador){

	    Colaborador colaboradorNovo = colaboradorService.cadastrar(colaborador);
		return created(fromCurrentRequest()
						.path("/{id}")
						.buildAndExpand(colaboradorNovo.getId())
						.toUri())
				.body(colaboradorNovo);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> atualizar(@PathVariable("id") Integer id,@RequestBody Colaborador colaborador){
		colaborador.setId(id);
		colaboradorService.atualizar(colaborador);
		return ok().build();
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletar(@PathVariable("id") Integer id){
		colaboradorService.remover(id);
		return ok().build();
	}
	
	@GetMapping("/cpf/{cpf}")
	public ResponseEntity<Colaborador> buscarPorId(@PathVariable("cpf") String cpf){
		return ok(colaboradorService.buscarPor(cpf));
	}
}
