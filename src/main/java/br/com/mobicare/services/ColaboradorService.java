package br.com.mobicare.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mobicare.model.Colaborador;
import br.com.mobicare.repository.ColaboradorRepository;
import br.com.mobicare.services.exception.ObjetoNaoEncontradoException;

@Service
public class ColaboradorService {
	
	@Autowired
	protected ColaboradorRepository repository;
	
	public List<Colaborador> buscarTodos(){
		List<Colaborador> colabodores = repository.findAll();        
        return colabodores;

	}
	
	public Colaborador buscarPor(Integer id) {
		Optional<Colaborador> obj = repository.findById(id);
		if(!obj.isPresent()) {
			throw new ObjetoNaoEncontradoException("O colaborador que você está procurando não existe");
		}
		
		return obj.get();
	}
	
	public void atualizar(Colaborador colaborador) {
		repository.save(colaborador);
	}
	
	public void remover(Integer id) {
		repository.delete(this.buscarPor(id));		
	}
	
	public Colaborador cadastrar(Colaborador colaborador) {		
		return repository.save(colaborador);
	}
	
	public Colaborador buscarPor(String cpf) {
		Colaborador colaborador = repository.findByCpf(cpf);
		return colaborador;
	}

}
